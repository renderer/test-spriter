package com.dongbat.example.spriter;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.Drawer;
import com.brashmonkey.spriter.LibGdx.LibGdxDrawer;
import com.brashmonkey.spriter.LibGdx.LibGdxLoader;
import com.brashmonkey.spriter.Player;
import com.brashmonkey.spriter.SCMLReader;
import com.brashmonkey.spriter.Timeline;

public class TestSpriter extends ApplicationAdapter {

  private Array<Player> players = new Array<Player>();
  private ShapeRenderer renderer;
  private SpriteBatch batch;
  private Drawer<Sprite> drawer;
  private LibGdxLoader loader;
  private OrthographicCamera cam;
  private BitmapFont font;
  private Data data;

  @Override
  public void create() {
    super.create();
    font = new BitmapFont();
    cam = new OrthographicCamera();
    cam.zoom = 1f;
    renderer = new ShapeRenderer();
    batch = new SpriteBatch();
    FileHandle handle = Gdx.files.internal("monster/basic_002.scml");
    data = new SCMLReader(handle.read()).getData();

    loader = new LibGdxLoader(data);
    loader.load(handle.file());

    drawer = new LibGdxDrawer(loader, batch, renderer);

    addMore();
  }

  private void addMore() {
    for (int i = 0; i < 100; i++) {
      Player player = new Player(data.getEntity(0));
      player.setScale(0.2f);
      player.setTime(MathUtils.random(1000));
      player.setPosition(MathUtils.random(- Gdx.graphics.getWidth() / 2, Gdx.graphics.getWidth() / 2), MathUtils.random(- Gdx.graphics.getHeight() / 2, Gdx.graphics.getHeight() / 2));
      players.add(player);
    }
  }

  @Override
  public void resize(int width, int height) {
    super.resize(width, height);

    cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    cam.position.set(0, 0, 0f);
    cam.update();
    renderer.setProjectionMatrix(cam.combined);
    batch.setProjectionMatrix(cam.combined);
  }

  private final Vector2 v = new Vector2();
  private final Vector3 v3 = new Vector3();

  @Override
  public void render() {
    super.render();

    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    if (Gdx.input.isKeyJustPressed(Input.Keys.PLUS)) {
      addMore();
    }

    batch.begin();

    for (Player player : players) {
      player.update();

      Timeline.Key.Bone head = player.getBone("head");
      v3.set(Gdx.input.getX(), Gdx.input.getY(), 0);
      cam.unproject(v3);
      if (v3.x > player.getX()) {
        if (player.flippedX() == -1) {
          player.flipX();
        }
      } else {
        if (player.flippedX() == 1) {
          player.flipX();
        }
      }
      v.set(v3.x, v3.y);
      float angle = v.sub(head.position.x, head.position.y).angle();
      if (player.flippedX() == 1) {
        angle -= 270;
        while (angle < 0) {
          angle += 360;
        }
        angle %= 360;
        angle = MathUtils.clamp(angle, 15, 130);
      } else {
        angle += 90;
        while (angle < 0) {
          angle += 360;
        }
        angle %= 360;
        angle = MathUtils.clamp(angle, 230, 345);
      }
      player.setBone("head", angle);
      drawer.draw(player);
    }
    font.draw(batch, Gdx.graphics.getFramesPerSecond() + "", 0, 0);

    batch.end();
  }

  @Override
  public void dispose() {
    super.dispose();

    renderer.dispose();
    loader.dispose();
  }
}
